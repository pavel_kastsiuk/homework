﻿using System;

namespace HW_3
{
    class Program
    {
        static void Main(string[] args)
        {
            var r = new Rectangle(12, 23);
            var t = new Trapezoid(4, 5, 8, 9, 5);
            var rh = new Rhombus(23, 40, 100);

            Console.Write("Прямоугольник:\nСторона А = {0}\nСторона В = {1}\n" +
                          "Площадь: {2}\nПериметр: {3}\nДлина диагонали: {4}\n",
                r.A, r.B, r.Square(), r.Perimetr(), r.Diagonal());
            Console.WriteLine("----------------------------------");

            Console.Write("Трапеция:\nСторона А = {0}\nСторона В = {1}\n" +
                "Сторона C = {6}\nСторона D = {7}\nВысота: {5}\n\nПлощадь: {2}\n" +
                "Периметр: {3}\nДлина диагонали1: {4}\nДлина диагонали2: {8}\n\n",
              t.A, t.B, t.Square(), t.Perimetr(), t.Diagonal(), t.H, t.C, t.D,t.Diagonal2());
            Console.WriteLine("----------------------------------");

            Console.Write("Ромб:\nСторона А = {0}\nУгол1 = {1}\nУгол2 = {2}\n\n" +
                "Площадь: {3}\nПериметр: {4}\nДлина диагонали1: {5}\nДлина диагонали2: {6}\n",
                rh.A, rh.AngleA, rh.AngleB, rh.Square(), rh.Perimetr(), rh.Diagonal(), rh.Diagonal2());

            Console.ReadLine();
        }
    }
}
