﻿using System;

namespace HW_3
{
    public abstract class Quadrilateral
    {
        public abstract double A { get; set; }
        public abstract double Square();
        public abstract double Perimetr();
        public abstract double Diagonal();
    }
    public sealed class Rectangle : Quadrilateral
    {
        public Rectangle(double a, double b)
        {
            this.A = a;
            this.B = b;
        }
        public override double A { get; set; }
        public double B { get; set; }
        public override double Square()
        {
            return A*B;
        }
        public override double Perimetr()
        {
            return 2*(A + B);
        }
        public override double Diagonal()
        {
            return Math.Sqrt((A*A) + (B*B));
        }
    }
    public sealed class Trapezoid : Quadrilateral
    {
        public Trapezoid(double a, double b, double c, double d, double h)
        {
            this.A = a;
            this.B = b;
            this.C = c;
            this.D = d;
            this.H = h;
        }
        public double H { get; set; }
        public override double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }
        public double D { get; set; }
        public override double Square()
        {
            return ((A + B)/2)*H;
        }
        public override double Perimetr()
        {
            return A + B + C + D;
        }
        public override double Diagonal()
        {
            return Math.Sqrt(A*A + D*D - 2*A*Math.Sqrt(D*D - H*H));
        }
        public double Diagonal2()
        {
            return Math.Sqrt(A*A + C*C - 2*A*Math.Sqrt(C*C - H*H));
        }
    }
    public sealed class Rhombus : Quadrilateral
    {
        public Rhombus(double a, double angA, double angB)
        {
            this.A = a;
            this.AngleA = angA;
            this.AngleB = angB;
        }
        public override double A { get; set; }
        public double AngleA { get; set; }
        public double AngleB { get; set; }
        public override double Square()
        {
            return (A*A)*Math.Sin(AngleA);
        }
        public override double Perimetr()
        {
            return 4*A;
        }
        public override double Diagonal()
        {
            return A*Math.Sqrt(2 - (2*Math.Cos(AngleB)));
        }
        public double Diagonal2()
        {
            return A * Math.Sqrt(2 - (2 * Math.Cos(AngleA)));
        }
    }

}
