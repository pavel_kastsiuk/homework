﻿using System;

namespace HW_4_2
{
    class MyStack
    {
        private Container _firstItem;

        public void Push(Container item)
        {
            item.Next = _firstItem;
            _firstItem = item;
        }

        public Container Pop()
        {
            var item = _firstItem;
            {
                if (item != null)
                    _firstItem = _firstItem.Next;
                if (item != null) item.Next = null;
            }
            return item;
        }
        public void ShowData()
        {
            var item = _firstItem;
            while (item != null)
            {
                Console.WriteLine(item.Data);
                item = item.Next;
            }
        }
    }
}
