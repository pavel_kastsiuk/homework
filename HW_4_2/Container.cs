﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_4_2
{
    class Container
    {
        public string Data { get; set; }
        public Container Next { get; set; }
    }
}
