﻿using System;

namespace HW_4_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var songList = new MyStack();

            string userInput;
            do
            {
                userInput = Console.ReadLine();
                songList.Push(new Container { Data = userInput });
            } while (!string.IsNullOrEmpty(userInput));

            Console.WriteLine("You've entered SongList: ");
            songList.ShowData();

            var item = songList.Pop();
            while (item != null)
            {
                Console.WriteLine(item.Data);
                item = songList.Pop();
            }

            Console.ReadLine();
        }
    }
}
