﻿using System;
using System.IO;
using System.Net.NetworkInformation;
using System.Runtime.Serialization;
using System.Threading;
using NAudio;
using NAudio.Wave;

namespace BenimTrack
{
    public delegate void LoggerDelegate(string message, string name);

    [DataContract]
    public class Track
    {
        public event LoggerDelegate PlayEvent;
        public event LoggerDelegate StopEvent;
        public event LoggerDelegate LogMessage;
        
        private string _name;

        [DataMember]
        public string Name
        {
            get { return _name; }
            set
            {
                if (!String.IsNullOrEmpty(value) &&
                    value.Length > 256)
                    _name = value.Substring(0, 256);
                else
                {
                    _name = value;
                }
            }
        }

        [DataMember]
        public string Artist { get; set; }
        [DataMember]
        public Genre Genre { get; set; }
        [DataMember]
        public DateTime Release { get; set; }
        public Track(string name, string artist, Genre genre, DateTime release)
        {
            _name = name;
            Artist = artist;
            Genre = genre;
            Release = release;
        }
        public Track(string name)
        {
            _name = name;
        }

        public void Play(string path)
        {
            if (PlayEvent != null && LogMessage != null)
            {
                PlayEvent("Началось проигрывание!", Name);
                LogMessage("Остановка проигрывания через 5 сек!!!", Name);
            }
            using (var ms = File.OpenRead(string.Join(@"\", path, Name)))
            using (var rdr = new Mp3FileReader(ms))
            using (var wavStream = WaveFormatConversionStream.CreatePcmStream(rdr))
            using (var baStream = new BlockAlignReductionStream(wavStream))
            using (var waveOut = new WaveOut(WaveCallbackInfo.FunctionCallback()))
            {
                waveOut.Init(baStream);
                waveOut.Play();
                var pauseStop = Console.ReadLine();
                //Thread.Sleep(5000);
                Console.WriteLine("Нажмите -s- чтобы остановить проигрывание");
                switch (pauseStop)
                {
                    case "s":
                        waveOut.Stop();
                        break;
                }
            }
            Console.WriteLine("Песня {0} играет", Name);
        }
        public void Stop()
        {
            if (StopEvent != null)
            {
                StopEvent("Проигрывание остановлено", Name);
            }
            Console.WriteLine("Трек {0} остановлен", Name);
        }
    }
}