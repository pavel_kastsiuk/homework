﻿using System;
using System.IO;
using System.Threading;

namespace BenimAlbum
{
    public static class Logger
    {
        //static readonly object LockObj = new object(); //LOCK
        private static readonly Mutex Mut = new Mutex();
        public static void Log(string message, string name)
        {
            //lock (LockObj)
            Mut.WaitOne();
            {
                Console.WriteLine("Лог файл записан");

                using (var sw = new StreamWriter("logger.txt", true))
                {
                    sw.Write("{0} - {1} - {2}\r\n", DateTime.Now, message, name);
                }
            }
            Mut.ReleaseMutex();
        }
        public static void Error(string message)
        {
            //lock (LockObj)
            Mut.WaitOne();
            {
                Console.WriteLine("Ошибка! Записана в память!");

                using (var sw = new StreamWriter("logger.txt", true))
                {
                    sw.Write("{0} - {1}\r\n", DateTime.Now, message);
                }
            }
            Mut.ReleaseMutex();
        }
        public static void LogError(string message)
        {
            Mut.WaitOne();
            {
                Console.WriteLine("Ошибка создания Playlist\nДанная ошибка записана в лог");

                using (var sw = new StreamWriter("logger.txt", true))
                {
                    sw.Write("{0} - {1}\r\n", DateTime.Now, message);
                }
            }
            Mut.ReleaseMutex();
        }
        public static void LogMessage(string message, string name)
        {
            Mut.WaitOne();
            {
                Console.WriteLine("ОСТАНОВКА {0} через 5 сек!", name);

                using (var sw = new StreamWriter("logger.txt", true))
                {
                    sw.Write("{0} - {1}\r\n", DateTime.Now, message);
                }
            }
            Mut.ReleaseMutex();
        }
    }
}
