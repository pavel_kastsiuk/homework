﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using BenimTrack;

namespace BenimAlbum
{
    [DataContract]
    public class Album
    {
        [DataMember]
        private readonly List<Track> _tracks = new List<Track>();
        public const int Capacity = 5;
        public string AlbumPath { get; set; }
        public Album(string path)
        {
            AlbumPath = path;
        }

        [DataMember]
        private string _albumName;
        public string AlbumName
        {
            get { return _albumName; }
            set
            {
                if (!String.IsNullOrEmpty(value) &&
                    value.Length > 256)
                    _albumName = value.Substring(0, 256);
                else
                {
                    _albumName = value;
                }
            }
        }
        public void AddTrack(Track track) //push
        {
            try
            {
                if (_tracks.Count > Capacity)
                    throw new AlbumFullException();

                _tracks.Add(track);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public Track GetTrack() //pop
        {
            var lastTrack = _tracks.Count - 1;
            var trackToReturn = _tracks[lastTrack];
            _tracks.Remove(trackToReturn);
            return trackToReturn;
        }
        public void Play()
        {
            while (!AlbumIsEmpty())
            {
                var track = GetTrack();
                track.Play(AlbumPath);
            }
        }
        public bool AlbumIsEmpty()
        {
            return _tracks.Count == 0;
        }
    }
}
