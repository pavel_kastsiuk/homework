﻿using System;
using BenimTrack;

namespace BenimAlbum
{
    public class AlbumFullException : Exception
    {
        private const string RangeMessage = "Нельзя добавить еще одну песню!";

        public AlbumFullException() 
            : base(RangeMessage) { }

        public AlbumFullException(string message)
            : base(message) { }

        public AlbumFullException(string message, Exception inner)
            : base(message, inner) { }
    }

    public class AlbumError : Exception
    {
        private const string RangeMessage = "Ошибка в создании альбома";

        public AlbumError()
            : base(RangeMessage) { }

        public AlbumError(string message)
            : base(message) { }
        public AlbumError(string message, Exception inner)
            : base(message, inner) { }
    }
}
