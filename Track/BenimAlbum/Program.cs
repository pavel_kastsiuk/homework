﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using BenimTrack;

namespace BenimAlbum
{
    class Program
    {
        private static void Main()
        {
            #region create list ByHand
            //var album = new Album() { AlbumName = "Imza" };

            //album.AddTrack(new Track("Bitse de Gitsek", "Sila", Genre.Pop, DateTime.Now));
            //album.AddTrack(new Track("Masumum", "Askin Nur Yengi", Genre.Rock, DateTime.Now));
            //album.AddTrack(new Track("Yara Bende", "Zerrin Ozer", Genre.Claasic, DateTime.Now));
            //album.AddTrack(new Track("Bana Biraz Renk Ver", "Tarkan", Genre.Pop, DateTime.Now));
            //album.AddTrack(new Track("Ne Çok", "Sila", Genre.Rock, DateTime.Now));
            //album.AddTrack(new Track("Yoruldum", "Sila", Genre.Pop, DateTime.Now));

            //Album album;
            #endregion
            var album = new Album(@"D:\BenimMuzik");
            try
            {
                var di = Directory.GetFiles(album.AlbumPath);
                var fc = di.Length;
                

                for (var i = 0; i < fc; i++)
                {
                    var songName = new FileInfo(di[i]);
                    var sn = songName.Name;

                    //var mp3File = TagLib.File.Create(di[i]);
                    //var art = mp3File.Tag.FirstAlbumArtist;
                    ////var gen = mp3File.Tag.FirstGenre;
                    ////var rel = mp3File.Tag.Year;

                    album.AddTrack(new Track(sn));
                }

                for (var i = 0; i <= Album.Capacity; i++)
                {
                    StartToEnd(album);                            //метод пробега по всем песням для Play & Stop
                }
            }
            catch (Exception)
            {
                Logger.LogError(string.Format("Неправильно указана папка {0}", album.AlbumPath));
            }
            #region Serialize/Deserialize
            //var ds = new NetDataContractSerializer();

            //using (Stream s = File.Create("album.xml"))
            //{
            //    ds.Serialize(s, album);                     //метод сериализации
            //}

            //using (var s = File.OpenRead("album.xml"))
            //{
            //    album = (Album)ds.Deserialize(s);             //метод десериализации
            //}
            #endregion

            Console.ReadLine();
            Process.Start("logger.txt");
        }
        private static void StartToEnd(Album album)
        {
            var tr = album.GetTrack();
            
            tr.PlayEvent += Logger.Log;
            tr.StopEvent += Logger.Log;
            tr.LogMessage += Logger.LogMessage;
            tr.Play(album.AlbumPath);
            Thread.Sleep(1000);
            tr.Stop();
        }
    }
}
