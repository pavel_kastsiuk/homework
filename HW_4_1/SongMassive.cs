﻿using System;

namespace HW_4_1
{
    class SongMassive : IDisposable
    {
        private readonly string[] _songList = new[] {
        "01. Kenar Süsü",
        "02. Cebren Ve Hile İle",
        "03. Malum",
        "04. Uşak Makami",
        "05. Biktim",
        "06. ...Dan Sonra Featuring – Kenan Doğulu"};

        public void GetSongList()
        {
            foreach (var song in _songList)
            {
                Console.WriteLine(song);
            }
        }

        public void Dispose()
        {
            for (var i = 0; i < _songList.Length; i++)
            {
                _songList[i] = null;
            }
            Console.WriteLine("SongList array is empty");
        }
    }
}
