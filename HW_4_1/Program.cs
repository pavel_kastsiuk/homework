﻿using System;

namespace HW_4_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var song = new SongMassive();

            using(var song1 = new SongMassive())
            song1.GetSongList();
            Console.WriteLine(new string('-', 50));

            song.GetSongList();
            song.Dispose();

            Console.ReadLine();
        }
    }
}
