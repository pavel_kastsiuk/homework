﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var pe = new Person();
            var st = new Student();
            var te = new Teacher();
            var he = new HeadOfDepartment();

            pe.Speak();
            st.Speak();
            st.Work();
            te.Speak();
            te.Work();
            he.Speak();
            he.Work();
            he.Equals(st);

            he.BadGuy(true);

            Console.ReadLine();
        }
    }
}
