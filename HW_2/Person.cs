﻿using System;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace HW_2
{
    class Person
    {
        private const string Name = "Человек";

        public virtual void Speak()
        {
            Console.WriteLine("я {0}!!!", Name);
        }
    }

    class Student : Person
    {
        private const string Name = "Студент";
        private string _work;

        public void Work()
        {
            _work = "учусь";
            Console.WriteLine("я {0}!!!", _work);
        }

        public override void Speak()
        {
            Console.WriteLine("я {0}!!!", Name);
        }

    }
    class Teacher : Person
    {
        private const string Name = "Преподаватель";
        private string _work;

        public void Work()
        {
            _work = "преподаю";
            Console.WriteLine("я {0}!!!", _work);
        }

        public override void Speak()
        {
            Console.WriteLine("я {0}!!!", Name);
        }
    }
    class HeadOfDepartment : Person
    {
        private const string Name = "Заведующий";
        private string _work;

        public void Work()
        {
            _work = "заведую";
            Console.WriteLine("я {0}!!!", _work);
        }

        public override void Speak()
        {
            Console.WriteLine("я {0}!!!", Name);
        }

        public override bool Equals(object obj)
        {
            return obj.ToString() == "я заведую";
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public void BadGuy(bool bad)
        {
            if (bad == Equals(false))
            {
                Console.WriteLine("Он не Заведущий, а самозванец!");
                return;
            }
            Console.WriteLine("Он Заведущий!");
        }
    }
}
