﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_4_3
{
    class Song
    {
        public string SongName { get; set; }
        public string SongArtist { get; set; }
        public Song(string name, string artist)
        {
            SongName = name;
            SongArtist = artist;
        }
    }
}
