﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_4_3
{
    internal class Program
    {
        private static void Main()
        {
            var list = new List<Song>();

            string userInput1;
            string userInput2;

            do
            {
                Console.WriteLine("Введите название композиции");
                userInput1 = Console.ReadLine();
                Console.WriteLine("Введите название исполнителя");
                userInput2 = Console.ReadLine();
                list.Add(new Song(userInput1, userInput2));
            } while (!string.IsNullOrEmpty(userInput1));

            foreach (var song in list)
            {
                Console.WriteLine(string.Join(" - ", song.SongName, song.SongArtist));
            }

            Console.ReadLine();
        }
    }
}

