using System;

namespace CA_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите радиус!");
            double rad;
            Double.TryParse(Console.ReadLine(), out rad);

            var square = new MathFormula(rad);

            Console.WriteLine("Площадь " + square.S());
            Console.WriteLine("Площадь при радиусе 13" + square.S(13));
            Console.WriteLine("Периметр " + square.L());

            Console.ReadLine();
        }
    }
}
