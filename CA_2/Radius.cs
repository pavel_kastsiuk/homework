using System;

namespace CA_2
{
    class MathFormula
    {
        private double _r;
        private double _pi = Math.PI;

        public double Pi
        {
            get { return _pi; }
        }

        public double R
        {
            get { return _r; }
            set
            {
                if (value <= 0)
                {
                    _r = 15;
                    Console.WriteLine("Вы вели неверную переменную. " +
                        "Радиус равняется " + _r);
                }
                else _r = value;
            }
        }
        public MathFormula(double radius)
        {
            R = radius;
        }
        public double S()
        {
            return Pi*(R*R);
        }
        public double S(double radius)
        {
            return Pi * (radius * radius);
        }
        public double L()
        {
            return 2*Pi*R;
        }
    }
}
